opm-material (2022.10+ds-4) unstable; urgency=medium

  * d/(tests/)control: Limit the architectures to known working ones.

 -- Markus Blatt <markus@dr-blatt.de>  Fri, 24 Mar 2023 09:33:42 +0100

opm-material (2022.10+ds-3) unstable; urgency=medium

  * d/control: Removed unneeded git from Build-Depends.

 -- Markus Blatt <markus@dr-blatt.de>  Wed, 08 Mar 2023 23:37:43 +0100

opm-material (2022.10+ds-2) experimental; urgency=medium

  * d/control: Bump standards version to 4.6.2 (no-change)

 -- Markus Blatt <markus@dr-blatt.de>  Mon, 09 Jan 2023 11:35:45 +0100

opm-material (2022.10+ds-1) unstable; urgency=medium

  * New upstream version 2022.10
  * d/control: Bump version of OPM dependency to current one.

 -- Markus Blatt <markus@dr-blatt.de>  Tue, 15 Nov 2022 16:18:32 +0100

opm-material (2022.10~rc2+ds-1) experimental; urgency=medium

  * New upstream release candidate 2022.10-rc2
  * d/tests: depend on same version of packages for autopkgtest.

 -- Markus Blatt <markus@dr-blatt.de>  Tue, 08 Nov 2022 20:31:49 +0100

opm-material (2022.10~rc1+ds-1) experimental; urgency=medium

  * New upstream release candidate 2022.10-rc1
  * d/control: Depend on OPM 2022.10.
  * d: Updated copyright

 -- Markus Blatt <markus@dr-blatt.de>  Fri, 04 Nov 2022 09:59:33 +0100

opm-material (2022.04+ds-2) unstable; urgency=medium

  * Changed version in dune.module to 2022.04

 -- Markus Blatt <markus@dr-blatt.de>  Thu, 12 May 2022 14:28:02 +0200

opm-material (2022.04+ds-1) unstable; urgency=medium

  * New final upstream release 2022.04
  * debian/watch: Advertise repackaged tarball

 -- Markus Blatt <markus@dr-blatt.de>  Thu, 12 May 2022 14:27:16 +0200

opm-material (2022.04~rc1-2) experimental; urgency=medium

  * d/rules: Fix OPM version dependency

 -- Markus Blatt <markus@dr-blatt.de>  Thu, 28 Apr 2022 12:02:05 +0200

opm-material (2022.04~rc1-1) experimental; urgency=medium

  * New upstream release candidate 2022.04-rc1
  * d/control: Use debhelper-compat (= 12) for older Ubuntu support
  * Dropped patches incorporated in upstream
  * d/control: Depend on OPM 2022.04
  * Make new co2brinepvt and it's man page part of libopm-material-dev
  * Fix installation of man page.
  * Fixes spelling (amount -> amount)
  * d/tests: Removed dependency on libpython3.9-dev

 -- Markus Blatt <markus@dr-blatt.de>  Thu, 28 Apr 2022 09:45:14 +0200

opm-material (2021.10-6) unstable; urgency=medium

  * d/tests: Added dependency on libpython3.9-dev to allow migration

 -- Markus Blatt <markus@dr-blatt.de>  Fri, 22 Apr 2022 13:31:20 +0200

opm-material (2021.10-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Markus Blatt ]
  * d/rules: Facilitate building with pbuilder without local libopm-common-dev
  * d/control: Added libatlas-base-dev to Build-Depends

 -- Markus Blatt <markus@dr-blatt.de>  Fri, 01 Apr 2022 13:00:43 +0200

opm-material (2021.10-4) unstable; urgency=medium

  * d/control: Skip unnecessary dependency on libdune-istl-dev

 -- Markus Blatt <markus@dr-blatt.de>  Sat, 12 Mar 2022 21:16:32 +0100

opm-material (2021.10-3) unstable; urgency=medium

  * d/control: Try building on all architectures
  * d/tests: Added basic autpkgtest
  * d/tests: Added basic autpkgtest
  * d/copyright: Added more (former) members to OPM development team.

 -- Markus Blatt <markus@dr-blatt.de>  Tue, 08 Mar 2022 15:52:53 +0100

opm-material (2021.10-2) unstable; urgency=medium

  * d/control: Stripped unnecessary Build-Depends on g++ gfortran
  * d/control: Bumped to Standards version 4.6.0

 -- Markus Blatt <markus@dr-blatt.de>  Wed, 26 Jan 2022 12:45:03 +0100

opm-material (2021.10-1) unstable; urgency=medium

  * Initial release (Closes: #987381)

 -- Markus Blatt <markus@dr-blatt.de>  Tue, 16 Nov 2021 16:18:36 +0100
